import * as Sentry from '@sentry/browser'

const dsnBlock = document.getElementById('sentry-dsn')

if (dsnBlock !== null) {
  Sentry.init({
    dsn: JSON.parse(dsnBlock.textContent),
  })
}
