# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import datetime
from datetime import timedelta

import freezegun
import pytest
from django.core.exceptions import ValidationError
from django.utils.timezone import now

from accounts.factories import UserFactory
from hunts import cooldown
from hunts.factories import TeamMemberFactory, PuzzleFactory, GuessFactory, EpisodeFactory
from hunts.models import TeamRole


@pytest.fixture
def user(event):
    return TeamMemberFactory(team__role=TeamRole.PLAYER)


@pytest.fixture
def team(user):
    return user.teams.get()


@pytest.fixture
def puzzle(event):
    return PuzzleFactory()


class TestGeneric:
    def test_init_provides_defaults(self):
        assert cooldown.ConstantDelay().delay == datetime.timedelta(seconds=5)
        assert cooldown.TeamWideDelay().delay == datetime.timedelta(seconds=5)
        cb = cooldown.ExponentialRampConstantDecay()
        assert cb.initial == 1
        assert cb.base == 2
        assert cb.decay == 240

    def test_broken_creation(self, event, caplog):
        episode = EpisodeFactory(cooldown_behaviour='TeamWideDelay')
        episode.cooldown_parameters = {'delay': 'invalid number'}
        with pytest.raises(ValidationError):
            episode.save()


class TestConstantDelay:
    def test_constant_delay(self, user, team, puzzle):
        delay = timedelta(seconds=1)
        cb = cooldown.ConstantDelay(delay=1)
        with freezegun.freeze_time() as dt:
            assert cb.may_guess(user, team, puzzle, now())
            cb.cooldown(user, team, puzzle, now())
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay / 2)
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay / 2)
            assert cb.may_guess(user, team, puzzle, now())


class TestTeamwideDelay:
    def test_single_user(self, user, team, puzzle):
        TeamMemberFactory(team=team)
        delay = timedelta(seconds=1)
        cb = cooldown.TeamWideDelay(delay=1)
        with freezegun.freeze_time() as dt:
            assert cb.may_guess(user, team, puzzle, now())
            GuessFactory(by=user, for_puzzle=puzzle)
            assert cb.cooldown(user, team, puzzle, now()) - now() == delay
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay / 2)
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay / 2)
            assert cb.may_guess(user, team, puzzle, now())

    def test_full_team(self, user, team, puzzle):
        for u in UserFactory.create_batch(3):
            team.members.add(u)
        users = team.members.all()
        delay = timedelta(seconds=1)
        cb = cooldown.TeamWideDelay(delay=1)
        with freezegun.freeze_time() as dt:
            # Make each user guess at delay intervals. This should be possible, and after each one, that user
            # should not be allowed to guess immediately after.
            for user in users:
                assert cb.may_guess(user, team, puzzle, now())
                GuessFactory(by=user, for_puzzle=puzzle)
                cb.cooldown(user, team, puzzle, now())
                assert not cb.may_guess(user, team, puzzle, now())
                dt.tick(delay)
            # Do the same a few more times and ensure that the delay for each user is equal to the team size * the delay
            for _ in range(3):
                for i, user in enumerate(users):
                    assert cb.may_guess(user, team, puzzle, now())
                    GuessFactory(by=user, for_puzzle=puzzle)
                    cd = cb.cooldown(user, team, puzzle, now()) - now()
                    assert cd == len(users) * delay, \
                        f'Guess cooldown was not team size * delay for user {i} in repeat{_}. It was {cd}'
                    assert not cb.may_guess(user, team, puzzle, now())
                    dt.tick(delay)

    def test_partial_team(self, user, team, puzzle):
        for u in UserFactory.create_batch(3):
            team.members.add(u)
        # All but one of the team will be guessing
        users = list(team.members.all())[:-1]
        delay = timedelta(seconds=1)
        cb = cooldown.TeamWideDelay(delay=1)
        with freezegun.freeze_time('12:00:00') as dt:
            # Make each user guess at delay intervals. This should be possible, and after each one, that user
            # should not be allowed to guess immediately after.
            for user in users:
                assert cb.may_guess(user, team, puzzle, now())
                GuessFactory(by=user, for_puzzle=puzzle)
                cb.cooldown(user, team, puzzle, now())
                assert not cb.may_guess(user, team, puzzle, now())
                dt.tick(delay)
            # Extra wait for fudge factor...
            dt.tick(delay)
            # Do the same a few more times and ensure that the delay for each user is equal to the number of *actively
            # guessing* players * delay; hence teams who aren't all present aren't penalised
            for _ in range(3):
                for i, user in enumerate(users):
                    assert cb.may_guess(user, team, puzzle, now())
                    GuessFactory(by=user, for_puzzle=puzzle)
                    cd = cb.cooldown(user, team, puzzle, now()) - now()
                    assert cd == len(users) * delay,\
                        f'Guess cooldown was not active users * delay for user {i+1} in repeat {_+1}. It was {cd}'
                    assert not cb.may_guess(user, team, puzzle, now())
                    dt.tick(delay)


class TestERCD:
    def test(self, user, team, puzzle):
        cb = cooldown.ExponentialRampConstantDecay(
            initial=2, base=1.5, decay=5
        )
        with freezegun.freeze_time() as dt:
            assert cb.may_guess(user, team, puzzle, now())
            # Guess initially: delay should be equal to initial
            GuessFactory(by=user, for_puzzle=puzzle)
            delay = cb.cooldown(user, team, puzzle, now()) - now()
            assert delay.total_seconds() == 2
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay)
            assert cb.may_guess(user, team, puzzle, now())

            # Guess as soon as possible: delay should be initial*base
            GuessFactory(by=user, for_puzzle=puzzle)
            delay = cb.cooldown(user, team, puzzle, now()) - now()
            assert delay.total_seconds() == 2 * 1.5
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay)
            assert cb.may_guess(user, team, puzzle, now())

            # Guess as soon as possible: delay should be initial*base*base
            GuessFactory(by=user, for_puzzle=puzzle)
            delay = cb.cooldown(user, team, puzzle, now()) - now()
            assert delay.total_seconds() == 2 * 1.5 * 1.5
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay)
            assert cb.may_guess(user, team, puzzle, now())

            # Wait an extra decay/2 seconds: delay should have reduced by half before ramping again
            dt.tick(timedelta(seconds=2.5))
            GuessFactory(by=user, for_puzzle=puzzle)
            delay = cb.cooldown(user, team, puzzle, now()) - now()
            assert delay.total_seconds() == (2 * 1.5 * 1.5) / 2 * 1.5
            assert not cb.may_guess(user, team, puzzle, now())
            dt.tick(delay)
            assert cb.may_guess(user, team, puzzle, now())

    def test_defaults(self, user, team, puzzle):
        cb = cooldown.ExponentialRampConstantDecay()
        with freezegun.freeze_time() as dt:
            start = now()
            assert cb.may_guess(user, team, puzzle, now())
            # Guess ten times as fast as possible
            for _ in range(10):
                GuessFactory(by=user, for_puzzle=puzzle)
                delay = cb.cooldown(user, team, puzzle, now()) - now()
                dt.tick(delay)

            # Time taken is high but not insane.
            assert cb.may_guess(user, team, puzzle, now())
            assert now() - start == timedelta(minutes=17, seconds=3)

            # Having already waited for the cooldown, wait another 120 seconds; half the default decay time
            dt.tick(timedelta(seconds=120))
            GuessFactory(by=user, for_puzzle=puzzle)
            # Check the delay is again something high but reasonable
            delay = cb.cooldown(user, team, puzzle, now()) - now()
            assert delay == timedelta(minutes=8, seconds=32)

            # Wait for the full cooldown + decay time
            dt.tick(delay + timedelta(seconds=240))
            assert cb.may_guess(user, team, puzzle, now())
            GuessFactory(by=user, for_puzzle=puzzle)
            # Check the delay is back to the initial value
            delay = cb.cooldown(user, team, puzzle, now()) - now()
            assert delay == timedelta(seconds=1)

    def test_guessing_double_decay_rate(self, user, team, puzzle):
        cb = cooldown.ExponentialRampConstantDecay()
        with freezegun.freeze_time() as dt:
            # Guess many times, waiting for half the decay time each time
            for _ in range(50):
                assert cb.may_guess(user, team, puzzle, now())
                GuessFactory(by=user, for_puzzle=puzzle)
                cb.cooldown(user, team, puzzle, now()) - now()
                dt.tick(timedelta(seconds=cb.initial + cb.decay * 0.5))

    def test_guessing_too_fast(self, user, team, puzzle):
        cb = cooldown.ExponentialRampConstantDecay()
        with freezegun.freeze_time() as dt:
            # Guess many times, waiting for a bit less than the decay time each time
            for _ in range(35):
                if not cb.may_guess(user, team, puzzle, now()):
                    break
                GuessFactory(by=user, for_puzzle=puzzle)
                cb.cooldown(user, team, puzzle, now()) - now()
                dt.tick(timedelta(seconds=cb.initial + cb.decay * 0.45))
            else:
                raise AssertionError('Was able to guess faster than double decay rate')

    def test_cap(self, user, team, puzzle):
        cb = cooldown.ExponentialRampConstantDecay(initial=5, cap=5)
        with freezegun.freeze_time() as dt:
            for _ in range(20):
                assert cb.may_guess(user, team, puzzle, now())
                GuessFactory(by=user, for_puzzle=puzzle)
                delay = cb.cooldown(user, team, puzzle, now()) - now()
                assert delay == datetime.timedelta(seconds=5)
                dt.tick(delay)
