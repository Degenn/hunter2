/* global reactJsonForm */

let cooldownBehaviourDescriptions = {}
let cooldownParametersSchemas = {}

window.addEventListener('load', function () {
  let select = document.getElementById('id_cooldown_behaviour')
  fetch(
    select.dataset.infoUrl,
  ).then(
    res => res.json(),
  ).then(info => {
    cooldownBehaviourDescriptions = info.descriptions
    cooldownParametersSchemas = info.schemas
    updateBehaviourDescription(select.parentElement, info.descriptions[select.value])
  }).catch(error => {
    let parametersDiv = document.getElementById('id_cooldown_parameters_jsonform')
    parametersDiv.innerText = `Error getting behaviour schemas: ${error.name}. See console.`
    throw error
  })
  select.addEventListener('change', onCooldownBehaviourChange)
})

function updateBehaviourDescription(container, desc) {
  let help = container.querySelector('.cooldown-behaviour-description')
  if (!help) {
    let node = document.createElement('div')
    node.className = 'help cooldown-behaviour-description'
    help = container.appendChild(node)
  }
  help.innerText = desc
}

function onCooldownBehaviourChange(e) {
  let schema = cooldownParametersSchemas[e.srcElement.value]
  updateBehaviourDescription(e.srcElement.parentElement, cooldownBehaviourDescriptions[e.srcElement.value])
  let form = reactJsonForm.getFormInstance('id_cooldown_parameters_jsonform')
  form.update({
    schema: schema,
  })
}