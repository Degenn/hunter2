import 'bootstrap/js/dist/collapse'
import { Collapse } from 'bootstrap'

import 'hunter2/js/base'

import '../scss/stats.scss'

const prefix = 'stats_expand_'

window.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('.collapse').forEach(elt => elt.addEventListener('hidden.bs.collapse', function() {
    localStorage.setItem(prefix + this.id, 'false')
  }))

  document.querySelectorAll('.collapse').forEach(elt => elt.addEventListener('shown.bs.collapse', function() {
    localStorage.setItem(prefix + this.id, 'true')
  }))

  document.querySelectorAll('.collapse').forEach(elt => {
    if (localStorage.getItem(prefix + elt.id) === 'true') {
      new Collapse(elt, {
        show: true,
      })
    }
  })
})
