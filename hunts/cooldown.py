# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import abc
import datetime

from django.core.cache import caches
from django import forms

REGISTRY = {}


def get_behaviour(name):
    try:
        return REGISTRY[name]
    except KeyError:
        raise KeyError(f'Name {name} is not registered as a cooldown behaviour name')


class CooldownBehaviour(abc.ABC):
    _cooldown_store = caches['guess_cooldowns']
    friendly_name: str
    description: str
    schema: dict

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__()
        required_classattrs = ('friendly_name', 'description', 'schema')
        for attr in required_classattrs:
            if not hasattr(cls, attr):
                raise TypeError(f'Cooldown behaviour subclasses must have a {attr} attribute')

        REGISTRY[cls.__name__] = cls

    def __init__(self, **kwargs):
        defaults = {
            k: v['default'] for k, v in self.schema['keys'].items()
            if 'default' in v
        }
        defaults.update({
            k: v for k, v in kwargs.items()
            if k in self.schema['keys']
        })
        self.setup(**defaults)

    def setup(self, **kwargs):
        """Override to customise set up of the class and store parameters.

        This method will be passed the parameters specified in the schema"""
        self.__dict__.update(**kwargs)

    @abc.abstractmethod
    def delay_for(self, user, team, puzzle, now) -> datetime.timedelta:
        """Compute and return a timedelta after which the user may guess again."""
        ...

    @classmethod
    def _cooldown_key(cls, user, team, puzzle):
        return f'{puzzle.episode.event.pk}/{puzzle.pk}/{team.pk}/{user.pk}'

    def end_time(self, user, team, puzzle):
        return self._cooldown_store.get(self._cooldown_key(user, team, puzzle) + '/end_time')

    def last_guessed(self, user, team, puzzle):
        return self._cooldown_store.get(self._cooldown_key(user, team, puzzle) + '/last_guessed')

    def may_guess(self, user, team, puzzle, now):
        """Return whether the user may guess yet.

        Note this method *only* checks the player cooldown: it is a requirement that
        this, the cooldown we report to the player, is accurate.
        """
        time = self.end_time(user, team, puzzle)
        if time is None:
            return True
        return now >= time

    def cooldown(self, user, team, puzzle, now):
        """Return the next time at which the user may guess and set it in storage"""
        time = now + self.delay_for(user, team, puzzle, now)
        self._cooldown_store.set(self._cooldown_key(user, team, puzzle) + '/last_guessed', now)
        self._cooldown_store.set(self._cooldown_key(user, team, puzzle) + '/end_time', time)
        return time


class ConstantDelay(CooldownBehaviour):
    friendly_name = 'Constant Delay'
    description = "Classic behaviour: Each player must wait the delay after guessing before guessing again."

    parameters_field = forms.IntegerField
    schema = {
        'type': 'object',
        'keys': {
            'delay': {
                'type': 'integer',
                'default': 5,
                'help_text': 'Delay in seconds between repeated guesses of one player',
                'required': True,
            }
        }
    }

    def setup(self, delay: int):
        self.delay = datetime.timedelta(seconds=delay)

    def delay_for(self, user, team, puzzle, now):
        return self.delay


class TeamWideDelay(CooldownBehaviour):
    friendly_name = 'Simple Team Delay'
    description = "Limits the team as a whole to guess at most once per the " \
                  "delay duration (once each player's previous cooldown has expired)"

    schema = {
        'type': 'object',
        'keys': {
            'delay': {
                'type': 'integer',
                'default': 5,
                'help_text': 'Delay in seconds between repeated guesses of the team as a whole (subject to a small '
                             'inaccuracy)',
                'required': True,
            }
        }
    }

    def setup(self, delay: int):
        self.delay = datetime.timedelta(seconds=delay)

    def delay_for(self, user, team, puzzle, now):
        guesses = puzzle.guess_set.filter(
            by_team=team, given__gte=now - self.delay * team.members.count()
        ).order_by('-given')
        # Get the most recent (first in the above queryset) per player
        seen = set()
        guesses = [g for g in guesses if not (g.by in seen or seen.add(g.by))]
        return self.delay * max(1, len(guesses))


class ExponentialRampConstantDecay(CooldownBehaviour):
    friendly_name = 'Exponential-Constant'
    description = 'Delay ramps up exponentially, then decays over a constant length of time.'

    schema = {
        'type': 'object',
        'keys': {
            'initial': {
                'type': 'number',
                'default': 1,
                'help_text': 'Initial delay in seconds',
                'required': True,
            },
            'base': {
                'type': 'number',
                'default': 2,
                'help_text': 'Scaling factor',
                'required': True,
            },
            'decay': {
                'type': 'number',
                'default': 60 * 4,
                'help_text': 'Number of seconds after waiting the full delay after which the next delay will be back '
                             'to the initial delay. In practice users can guess at double this rate, even if the cap '
                             'is larger.',
                'required': True,
            },
            'cap': {
                'type': 'number',
                'default': 60 * 10,
                'help_text': 'Cooldown caps out at this number of seconds.',
                'required': True,
            }
        },
        'help_text': 'hello'
    }

    initial: float
    base: float
    decay: float
    cap: float

    def delay_for(self, user, team, puzzle, now):
        #         cur_delay           waited
        #   |-----------------|--------------------|
        #   ^--- prev guess   ^- allowed to guess  ^- now

        allowed = self.end_time(user, team, puzzle)
        if allowed is None:
            return datetime.timedelta(seconds=self.initial)

        last_guessed = self.last_guessed(user, team, puzzle)
        cur_delay = allowed - last_guessed if last_guessed is not None else datetime.timedelta(0)

        waited = (now - allowed).total_seconds()
        reduced_delay = max(
            datetime.timedelta(seconds=self.initial / self.base),
            cur_delay * (1 - (waited / self.decay))
        )

        return min(
            reduced_delay * self.base,
            datetime.timedelta(seconds=self.cap),
        )
