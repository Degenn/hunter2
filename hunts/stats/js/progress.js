import {
  Chart,
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Tooltip,
} from 'chart.js'
import 'chartjs-adapter-luxon'
import zoomPlugin from 'chartjs-plugin-zoom'
import distinctColors from 'distinct-colors'

Chart.register(
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  TimeScale,
  Legend,
  Tooltip,
  zoomPlugin,
)

let puz0name = '(started)'

function generateTeamColours(episode_progress) {
  let team_set = new Set()
  for (let episode in episode_progress) {
    for (let team of episode_progress[episode].teams) {
      team_set.add(team.team_id)
    }
  }
  let colours = distinctColors({count: team_set.size, chromaMin: 10, chromaMax: 90, lightMin: 20, lightMax: 80})
  let teams = Array.from(team_set)
  let team_colours = new Map()
  for (let i = 0; i < teams.length; ++i) {
    team_colours.set(teams[i], colours[i])
  }
  return team_colours
}

function timesToChartForm(data, puzfn, puzzleAxis, dateAxis) {
  let times = data.puzzle_times
  let result = []
  for (let i = 0; i < times.length; ++i)
    result.push({[puzzleAxis]: puzfn(i), [dateAxis]: times[i].date})
  return result
}

function getChartDataSets(teamData, team_colours, axisLabels) {
  let puzFn = function (n) {
    return n
  }
  if (Object.prototype.hasOwnProperty.call(teamData, 'puzzle_names')) {
    puzFn = function(n) {
      return (n > 0) ? teamData.puzzle_names[n-1] : puz0name
    }
  }

  let teamChartDatasets = []
  for (let team of teamData.teams) {
    let colour = team_colours.get(team.team_id).css()
    teamChartDatasets.push({
      data: timesToChartForm(team, puzFn, ...axisLabels),
      backgroundColor: colour,
      borderColor: colour,
      borderWidth: 1,
      hoverBorderColor: colour,
      hoverBorderWidth: 4,
      fill: false,
      label: team.team_name,
      lineTension: 0,
      pointRadius: 2,
      pointHoverRadius: 2,
    })
  }
  return teamChartDatasets
}

function setAllHidden(canvas, hidden) {
  let chart = Chart.getChart(canvas)
  chart.data.datasets.forEach(dataset => {
    dataset.hidden = hidden
  })
  chart.update()
}

function toggleXAxis(isTime) {
  for (let canvas of document.getElementsByClassName('progress-graph')) {
    let chart = Chart.getChart(canvas)
    chart.destroy()
    createChart(canvas, isTime)
  }
}

const getOrCreateLegendList = (chart, id) => {
  const legendContainer = document.getElementById(id)
  let listContainer = legendContainer.querySelector('ul')

  if (!listContainer) {
    listContainer = document.createElement('ul')
    listContainer.style.display = 'flex'
    listContainer.style.flexDirection = 'row'
    listContainer.style.overflowX = 'scroll'
    listContainer.style.margin = '6pt 0 0 0'
    listContainer.style.padding = '1rem'
    listContainer.style.border = '1px solid #0002'
    listContainer.style.borderRadius = '3px'
    listContainer.style.fontSize = '9pt'

    legendContainer.appendChild(listContainer)
  }

  return listContainer
}

const htmlLegendPlugin = {
  id: 'htmlLegend',
  afterUpdate(chart, args, options) {
    const ul = getOrCreateLegendList(chart, options.containerID)

    // Remove old legend items
    while (ul.firstChild) {
      ul.firstChild.remove()
    }

    // Reuse the built-in legendItems generator
    let items = chart.options.plugins.legend.labels.generateLabels(chart)
    // Sort alphabetically to make it easier to find your own team
    items.sort((a, b) => {
      let al = a.text.toLowerCase()
      let bl = b.text.toLowerCase()
      if (al === bl) {
        return 0
      }
      if (al < bl) {
        return -1
      } else {
        return 1
      }
    })

    items.forEach(item => {
      const li = document.createElement('li')
      li.style.alignItems = 'center'
      li.style.cursor = 'pointer'
      li.style.display = 'flex'
      li.style.flexDirection = 'row'
      li.style.marginLeft = '10px'
      li.style.flexShrink = '0'

      li.onclick = () => {
        const {type} = chart.config
        if (type === 'pie' || type === 'doughnut') {
          // Pie and doughnut charts only have a single dataset and visibility is per item
          chart.toggleDataVisibility(item.index)
        } else {
          chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex))
        }
        chart.update()
      }

      // Color box
      const boxSpan = document.createElement('span')
      boxSpan.style.background = item.fillStyle
      boxSpan.style.borderColor = item.strokeStyle
      boxSpan.style.borderWidth = item.lineWidth + 'px'
      boxSpan.style.display = 'inline-block'
      boxSpan.style.height = '20px'
      boxSpan.style.marginRight = '10px'
      boxSpan.style.width = '20px'

      // Text
      const textContainer = document.createElement('p')
      textContainer.style.color = item.fontColor
      textContainer.style.margin = '0'
      textContainer.style.padding = '0'
      textContainer.style.textDecoration = item.hidden ? 'line-through' : ''

      const text = document.createTextNode(item.text)
      textContainer.appendChild(text)

      li.appendChild(boxSpan)
      li.appendChild(textContainer)
      ul.appendChild(li)
    })
  },
}

let team_colours = generateTeamColours(window.episode_progress)

function createChart(canvas, timeOnX) {
  let puzAxis = {}

  let episode_data = window.episode_progress[canvas.dataset.episode]

  if (Object.prototype.hasOwnProperty.call(episode_data, 'puzzle_names')) {
    puzAxis.type = 'category'
    if (timeOnX) {
      puzAxis.labels = [puz0name].concat(episode_data.puzzle_names).reverse()
    } else {
      puzAxis.labels = [puz0name].concat(episode_data.puzzle_names)
    }
  } else {
    puzAxis.type = 'linear'
    puzAxis.ticks = {
      stepSize: 1,
      suggestedMin: 0,
      precision: 0,
      beginAtZero: true,
    }
  }

  let timeAxis = {
    type: 'time',
    ticks: {
      autoSkip: true,
      autoSkipPadding: 25,
    },
    time: {
      displayFormats: {
        hour: 'HH:mm',
        minute: 'HH:mm',
        second: 'HH:mm:ss',
      },
    },
  }

  let teamChartDatasets = getChartDataSets(episode_data, team_colours, timeOnX ? ['y', 'x'] : ['x', 'y'])

  if (timeOnX) {
    canvas.parentElement.classList.remove('tall')
  } else {
    canvas.parentElement.classList.add('tall')
  }

  let ctx = canvas.getContext('2d')
  new Chart(ctx, {
    type: 'line',
    data: {
      datasets: teamChartDatasets,
    },
    options: {
      maintainAspectRatio: false,
      hover: {
        mode: 'dataset',
      },
      layout: {
        padding: {
          left: 50,
          right: 50,
          top: 25,
          bottom: 25,
        },
      },
      scales: timeOnX ? {
        x: timeAxis,
        y: puzAxis,
      } : {
        x: puzAxis,
        y: timeAxis,
      },
      plugins: {
        htmlLegend: {
          containerID: `progress-legend-${canvas.dataset.episode}`,
        },
        legend: {
          display: false,
        },
        zoom: {
          pan: {enabled: true, modifierKey: 'ctrl'},
          zoom: {
            drag: {enabled: true},
            wheel: {enabled: true},
            pinch: {enabled: true},
            mode: 'xy',
          },
          limits: timeOnX ? {
            x: {min: 'original', max: 'original', minRange: 60000},
            y: {min: 0, minRange: 3},
          } : {
            x: {min: 0, minRange: 3},
            y: {min: 'original', max: 'original', minRange: 60000},
          },
        },
      },
    },
    plugins: [htmlLegendPlugin],
  })

}

for (let canvas of document.getElementsByClassName('progress-graph')) {
  createChart(canvas, true)

  let showAllButton = document.getElementById(`show-all-${canvas.dataset.episode}`)
  showAllButton.addEventListener('click', function() {
    setAllHidden(canvas, false)
  })

  let hideAllButton = document.getElementById(`hide-all-${canvas.dataset.episode}`)
  hideAllButton.addEventListener('click', function() {
    setAllHidden(canvas, true)
  })

  let resetZoomButton = document.getElementById(`reset-progress-zoom-${canvas.dataset.episode}`)
  resetZoomButton.addEventListener('click', () => {
    Chart.getChart(canvas).resetZoom()
  })
}

let xIsTimeButton = document.getElementById('progress-x-is-time')
xIsTimeButton.addEventListener('click', () => {toggleXAxis(true)})
let xIsProgressButton = document.getElementById('progress-x-is-progress')
xIsProgressButton.addEventListener('click', () => {toggleXAxis(false)})
