import {
  Chart,
  BarElement,
  LineElement,
  PointElement,
  LineController,
  BarController,
  CategoryScale,
  LinearScale,
  Tooltip,
} from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels'

Chart.register(
  BarElement,
  LineElement,
  PointElement,
  LineController,
  BarController,
  CategoryScale,
  LinearScale,
  Tooltip,
  ChartDataLabels,
)

function toggleSolvedDenominator(dataGetter) {
  for (let canvas of document.getElementsByClassName('completion-chart')) {
    let episode_data = JSON.parse(document.getElementById('episode-completion').textContent)[canvas.dataset.episode]
    let chart = Chart.getChart(canvas)
    chart.data.datasets[0].data = dataGetter(episode_data)
    chart.update()
  }
}

function getDataRelativeToHunt(episode_data) {
  return episode_data.puzzles.map(data => data.solved / Math.max(episode_data.started_any, 1))
}

function getDataRelativeToPuzzle(episode_data) {
  return episode_data.puzzles.map(data => data.solved / Math.max(data.started, 1))
}

for (let canvas of document.getElementsByClassName('completion-chart')) {
  let episode_data = JSON.parse(document.getElementById('episode-completion').textContent)[canvas.dataset.episode]

  let ctx = canvas.getContext('2d')

  let config = {
    type: 'bar',
    data: {
      labels: episode_data.puzzles.map(data => data.title),
      datasets: [{
        data: getDataRelativeToHunt(episode_data),
        backgroundColor: 'rgba(50, 50, 150, 0.5)',
        borderColor: 'black',
        borderWidth: 1,
        datalabels: {
          labels: {
            total: {
              formatter: function(value, ctx) {
                return episode_data.puzzles[ctx.dataIndex].solved
              },
              color: 'white',
              align: 'start',
              anchor: 'end',
            },
          },
        },
      }],
    },
    options: {
      layout: {
        padding: {
          left: 30,
          right: 30,
          top: 30,
          bottom: 30,
        },
      },
      scales: {
        y: {
          ticks: {
            format: {
              style: 'percent',
            },
          },
        },
      },
    },
  }
  new Chart(ctx, config)
}

let relativeToHuntButton = document.getElementById('completion-relative-to-hunt')
relativeToHuntButton.addEventListener('click', () => {toggleSolvedDenominator(getDataRelativeToHunt)})
let relativeToPuzzleButton = document.getElementById('completion-relative-to-puzzle')
relativeToPuzzleButton.addEventListener('click', () => {toggleSolvedDenominator(getDataRelativeToPuzzle)})
