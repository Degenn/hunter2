#  Copyright (C) 2022 The Hunter2 Contributors.
#
#  This file is part of Hunter2.
#
#  Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#  Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
#
#  This file is part of Hunter2.
#
#  Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#  Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
from django.db.models import Exists, OuterRef
from schema import Schema

from hunts.models import TeamPuzzleProgress, Episode, Puzzle
from hunts.stats.abstract import AbstractGenerator
from teams.models import Team


class CompletionGenerator(AbstractGenerator):
    """
    Generates ratios of puzzle completion
    """
    title = 'Puzzle Completion'
    version = 1
    admin = True

    schema = Schema({
        'episodes': {
            int: {
                'name': str,
                'started_any': int,
                'puzzles': [{
                    'title': str,
                    'started': int,
                    'solved': int,
                }]
            }
        }
    })

    def generate(self):
        if self.episode is not None:
            episodes = (self.episode, )
        else:
            episodes = Episode.objects.filter(no_stats=False)

        output = {}

        for episode in episodes:
            puzzles = Puzzle.objects.filter(episode=episode).seal()
            puzzle_completions = []
            # The largest rate can be used for scaling in the frontend
            started_max = 1
            solved_max = 0
            for puzzle in puzzles:
                started = TeamPuzzleProgress.objects.filter(puzzle=puzzle, late=False, start_time__isnull=False).count()
                solved = TeamPuzzleProgress.objects.filter(puzzle=puzzle, late=False, solved_by__isnull=False).count()

                if started_max * solved > solved_max * started:
                    started_max = started
                    solved_max = solved

                puzzle_completions.append({
                    'title': puzzle.title,
                    'started': started,
                    'solved': solved,
                })

            output[episode.id] = {
                'name': episode.name,
                'started_any': Team.objects.annotate(
                    started=Exists(
                        TeamPuzzleProgress.objects.filter(
                            team_id=OuterRef('id'),
                            start_time__isnull=False,
                            start_time__lt=episode.event.end_date
                        )
                    )
                ).filter(
                    started=True
                ).count(),
                'puzzles': puzzle_completions,
            }
        return {
            'episodes': output
        }
